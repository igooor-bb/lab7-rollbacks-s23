import psycopg2


price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
add_to_inventory = "INSERT INTO Inventory (username, product, amount) VALUES (%(username)s, %(product)s, %(amount)s) ON CONFLICT (username, product) DO UPDATE SET amount = Inventory.amount + %(amount)s"
register_transaction = "INSERT INTO CompletedTransactions (transaction_id, timestamp) VALUES (%(transaction_id)s, NOW())"

def get_connection():
    return psycopg2.connect(
        dbname="lab",
        user="postgres",
        password="1234",
        host="localhost",
        port=5432
    )

def buy_product(transaction_id, username, product, amount):
    obj = {"transaction_id": transaction_id, "product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                cur.execute("SELECT transaction_id FROM CompletedTransactions WHERE transaction_id = %(transaction_id)s", obj)
                if cur.fetchone() is not None:
                    raise Exception("Transaction already completed")

                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
                    
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")

                cur.execute(add_to_inventory, obj)
                if cur.rowcount != 1:
                    raise Exception("Failed to update inventory")
                
                cur.execute(register_transaction, obj)

                conn.commit()
            except (psycopg2.errors.CheckViolation, psycopg2.errors.RaiseException) as e:
                conn.rollback()
                if "balance" in str(e):
                    raise Exception("Bad balance")
                elif "in_stock" in str(e):
                    raise Exception("Product is out of stock")
                elif "Inventory limit reached" in str(e):
                    raise Exception("Inventory limit reached")

# Example usage:
import uuid
transaction_id = uuid.uuid4()
# buy_product(transaction_id, 'Alice', 'marshmello', 1)