# Lab7 - Rollbacks and reliability

## Part 1

In the given code, there are two separate transactions for updating the player's balance and the shop's stock. If any error occurs in the middle of these two transactions, it will lead to data inconsistency. To fix this issue, we can combine these two transactions into a single transaction using the 'with' statement.

## Part 2

First, create the `Inventory` table SQL:

```sql
CREATE TABLE Inventory (username TEXT, product TEXT, amount INT CHECK (amount >= 0), PRIMARY KEY (username, product), FOREIGN KEY (username) REFERENCES Player(username), FOREIGN KEY (product) REFERENCES Shop(product));
```

Second, create the trigger function in SQL:

```sql
CREATE OR REPLACE FUNCTION check_inventory_limit()
RETURNS TRIGGER AS $$
DECLARE
  total_items INT;
BEGIN
  SELECT SUM(amount) INTO total_items FROM Inventory WHERE username = NEW.username;
  IF total_items + NEW.amount > 100 THEN
    RAISE EXCEPTION 'Inventory limit reached';
  END IF;
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;
```

Then, create a trigger that calls this function before an update or insert operation on the Inventory table:

```sql
CREATE TRIGGER enforce_inventory_limit
  BEFORE INSERT OR UPDATE ON Inventory
  FOR EACH ROW
  EXECUTE FUNCTION check_inventory_limit();
```

Now, we update the function to handle inventory updates.

## Extra part (1/2)

In situations where unexpected failures occur (power outages or connection issues), the function might fail after committing the transaction. Retrying such requests could result in problems like double spending or incorrect inventory updates.

To address this, we can implement idempotent operations using a unique transaction identifier. This involves storing completed transactions with a unique identifier and checking if a transaction has already been completed before re-executing it. This ensures retried requests won't lead to duplicate or incorrect updates.

First, create a new table to store completed transactions:

```sql
CREATE TABLE CompletedTransactions (transaction_id UUID UNIQUE, timestamp TIMESTAMP);
```

Then, we update the function to check if the transaction has been completed before proceeding:

## Extra part (2/2)

In the example provided, a database operation decreases the balance, followed by a background task for actual money withdrawal using a message queue. To handle consistency in such scenarios, a few approaches can be used:

1. [Two-phase commit protocol (2PC)](https://en.wikipedia.org/wiki/Two-phase_commit_protocol): This distributed transaction method employs a coordinator to manage commits or rollbacks across different systems. It can be difficult to implement when external systems lack rollback support.

2. [Saga pattern](https://microservices.io/patterns/data/saga.html): This approach executes a series of local transactions, and if one fails, compensating transactions reverse the previous transactions' effects. This pattern is more suitable for systems with varying rollback capabilities, allowing for eventual consistency.

3. [Event-driven architecture](https://learn.microsoft.com/en-us/azure/architecture/guide/architecture-styles/event-driven): Operations are based on events and their corresponding event handlers. An event store maintains each event's state. If an event fails, an event handler takes corrective actions or retries the event, providing better fault tolerance and resilience.

4. [Idempotent operations](https://stripe.com/docs/api/idempotent_requests): Ensuring idempotency helps maintain consistency in case of retries. Unique identifiers or request tags can track and prevent duplicate operations across systems.
